package common

import (
	"io"
	"log"
	"os"
	"os/exec"
)

const krest = "k-rest"

// ExecCommand Makes the 'echo' command send the information in 'data' to a pipe(|) and 'jq' processes it later
func ExecCommand(data string) {
	//echo data | jq

	echoCmd := exec.Command("echo", data)
	jqCmd := exec.Command("jq")

	reader, writer := io.Pipe()

	echoCmd.Stdout = writer
	jqCmd.Stdin = reader
	jqCmd.Stdout = os.Stdout

	echoCmd.Start()
	jqCmd.Start()

	echoCmd.Wait()
	writer.Close()

	jqCmd.Wait()
	reader.Close()
}

func ReadJsonFile(pathFile string) *os.File {
	f, err := os.Open(pathFile)
	if err != nil {
		log.Println("Error Reading file", f)
		return nil
	}

	return f
}
