# Cli-Rest (krest)

**Requires** to work

- [jq](https://stedolan.github.io/jq/) 

## Usage  

```
cli-rest get http://127.0.0.1:8080/
cli-rest post http://127.0.0.1:8080/ file.json
cli-rest put http://127.0.0.1:8080/ file.json
cli-rest delete http://127.0.0.1:8080/id
```
  
![get](./img/cli-rest-get.png)
