package cli

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/kokegudiel2/cli-rest/client"
)

var Hello = &cobra.Command{
	Use: "hello",
	Run: func(cmd *cobra.Command, args []string) {
		println("Puto el que lo lea")
	},
}

var getMethod = &cobra.Command{
	Use:  "get",
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Println("Error Path Missing")
			return
		}

		client.Get(args[0])
	},
}

var postMethod = &cobra.Command{
	Use:  "post",
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Println("Error Path||Json Missing")
			return
		}

		client.Post(args[0], args[1])
	},
}

var putMethod = &cobra.Command{
	Use:  "put",
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Println("Error Path||Json Missing")
			return
		}

		client.Put(args[0], args[1])
	},
}

var deleteMethod = &cobra.Command{
	Use:  "delete",
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Println("Error Path Missing")
			return
		}

		client.Delete(args[0])
	},
}

func AddCommands() {
	RootCmd.AddCommand(Hello)
	RootCmd.AddCommand(getMethod)
	RootCmd.AddCommand(postMethod)
	RootCmd.AddCommand(putMethod)
	RootCmd.AddCommand(deleteMethod)
}
