package main

import (
	"log"

	"gitlab.com/kokegudiel2/cli-rest/cli"
)

func main() {
	log.Println("Tumadre.com")

	cli.AddCommands()

	if err := cli.RootCmd.Execute(); err != nil {
		log.Fatalln(err)
	}
}
