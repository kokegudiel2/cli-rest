package client

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/kokegudiel2/cli-rest/common"
)

func Get(path string) error {
	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		log.Println("Error - Method: GET", err)
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	c := &http.Client{}
	response, _ := c.Do(req)

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("Error - Method: GET", err)
		return err
	}

	common.ExecCommand(string(data))

	return nil
}

func Post(url, filePath string) error {
	f := common.ReadJsonFile(filePath)
	if f == nil {
		log.Println("Error - Method: POST")
		return errors.New("Error Parsing Json file")
	}
	defer f.Close()

	req, err := http.NewRequest(http.MethodPost, url, f)
	if err != nil {
		log.Println("Error - Method: POST", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	c := &http.Client{}
	response, err := c.Do(req)
	if err != nil {
		log.Println("Error - Method: POST", err)
		return err
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("Error - Method: POST", err)
		return err
	}

	common.ExecCommand(string(data))

	return nil
}

func Put(url, filePath string) error {
	f := common.ReadJsonFile(filePath)
	if f == nil {
		log.Println("Error - Method: PUT")
		return errors.New("Error Parsing Json file")
	}
	defer f.Close()

	req, err := http.NewRequest(http.MethodPut, url, f)
	if err != nil {
		log.Println("Error - Method: PUT", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	c := &http.Client{}
	response, err := c.Do(req)
	if err != nil {
		log.Println("Error - Method: PUT", err)
		return err
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("Error - Method: PUT", err)
		return err
	}

	common.ExecCommand(string(data))

	return nil
}

func Delete(url string) error {
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		log.Println("Error - Method: DELETE", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("Error - Method: DELETE", err)
		return err
	}

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("Error - Method: DELETE", err)
		return err
	}

	common.ExecCommand(string(data))

	return nil
}
